import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../views/layout/index.vue'
import Home from '../views/home/index.vue'
import Qa from '../views/qa/index.vue'
import Video from '../views/video/index.vue'
import My from '../views/my/index.vue'
import Login from '../views/login/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '/', // 默认子路由
        component: Home
      },
      {
        path: '/qa',
        component: Qa
      },
      {
        path: '/video',
        component: Video
      },
      {
        path: '/my',
        component: My
      }
    ]
  },
  {
    path: '/login',
    component: Login
  }
]

const router = new VueRouter({
  routes
})

export default router
